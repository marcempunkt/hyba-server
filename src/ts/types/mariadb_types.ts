export type Placeholder = any;

export enum TinyInt {
  FALSE = 0,
  TRUE = 1,
};

export interface OkPacket {
  affectedRows: number;
  insertId: number;
  warningStatus: number;
};


