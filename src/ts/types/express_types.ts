import type Config from "../../lib/config";
import type { Status } from "./db_types";

export interface ResponseBody {
    message: string;
}

export type AuthLocals = Record<"userId", number>;

/* export type AppLocals = { config: Config }; */
/* export type AppLocals = Record<"config", Config>; */

export interface UserResponse {
    id: number;
    username: string;
    email: string;
    register_date: number;
    status: Status;
};

export type Empty = {};
