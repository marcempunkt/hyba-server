import Joi, { ValidationResult } from "@hapi/joi";

export const loginValidation: (requestBody: { email: string; password: string }) => ValidationResult<{ email: string; password: string; }> = (requestBody) => {
  /**
   * Login Validation
   */
  let schema = {
    email: Joi.string().min(6).max(255).email().required(),
    password: Joi.string().min(8).max(1024).required()
  }
  return Joi.validate(requestBody, schema);
};

export const registerValidation: (requestBody: { username: string; email: string; password: string }) => ValidationResult<{ username: string, email: string; password: string; }> = (requestBody) => {
  /**
   * Register Validation
   */
  let schema = {
    username: Joi.string().alphanum().min(3).max(20).required(),
    email: Joi.string().min(6).max(255).email().required(),
    password: Joi.string().min(8).max(1024).required()
  }
  return Joi.validate(requestBody, schema);
};

const UserControllerUtils = {
  loginValidation,
  registerValidation,
};

export default UserControllerUtils;
