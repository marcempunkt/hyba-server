import { Request, Response } from "express";
import colors from "colors";
import { PoolConnection } from "mariadb";

import type { ResponseBody, UserResponse } from "../ts/types/express_types";
import pool from "../db/db";
import getIdFromToken from "../utils/getIdFromToken";
import getIdFromParam from "../utils/getIdFromParam";

const findbyUsername = async (
    req: Request<{ username: string }>,
    res: Response<UserResponse | ResponseBody>,
) => {
    /**
     * Get informationen of a user by its username
     * 
     * @param  {string}  username
     * 
     * @returns  {UserResponse}
     */
    let conn: PoolConnection | undefined;

    try {
        conn = await pool.getConnection();
        if (!conn) throw console.error(colors.red(`UserFindbyController.findbyUsername() => conn is undefined!`));

        const query: string = `SELECT id, username, register_date, status
                              FROM users 
                              WHERE username=?`;

        const queryResult = await conn.query(query, [req.params.username]);
        const user: UserResponse | undefined = queryResult[0];
        /* 400 : User not found! */
        if (!user) { return res.status(400).send({ message: "User not found!" }); }
        /* 200: Send User! */
        return res.send(user);

    } catch (err: unknown) {
        res.status(500).send({ message: "Could not find a user." });
        throw console.error(colors.red(`UserFindbyController.findbyUsername() => ${err}`));
    } finally {
        if (conn) conn.release();
    }
};

const findbyUserid = async (
    req: Request<{ userId: number }>,
    res: Response<UserResponse | ResponseBody>
) => {
    /**
     * Get informationen of a user by its username
     * 
     * @param  {number}  userId
     * 
     * @returns  {UserResponse}
     */
    let conn: PoolConnection | undefined;

    try {
        conn = await pool.getConnection();
        if (!conn) throw console.error(colors.red(`UserFindbyController.findbyUserid() => conn is undefined!`));

        const query: string = `SELECT id, username, email, register_date, status
                               FROM users 
                               WHERE id=?`;

        const queryResult = await conn.query(query, [req.params.userId]);
        const user: UserResponse | undefined = queryResult[0];
        /* 400 : User not found! */
        if (!user) { return res.status(400).send({ message: "User not found!" }); }
        /* 200: Send User! */
        return res.send(user);

    } catch (err: unknown) {
        res.status(500).send({ message: "Could not find a user." });
        throw console.error(colors.red(`UserFindbyController.findbyUserid() => ${err}`));
    } finally {
        if (conn) conn.release();
    }
};

const findbyToken = async (
    req: Request<{ token: string }>,
    res: Response<UserResponse | ResponseBody>
) => {
    /**
     * Get informationen of yourself by your authentication token
     * 
     * @param  {string}  token
     * 
     * @returns  {UserResponse}
     */
    const userId: number | null = getIdFromToken(req.params.token);
    // 401: Unauthorized
    if (!userId) return res.status(401).send({ message: "Unvalid authentication token." });

    let conn: PoolConnection | undefined;

    try {
        conn = await pool.getConnection();
        if (!conn) throw console.error(colors.red(`UserFindbyController.findbyToken => conn is undefined!`));

        const query = `SELECT email, username, id, register_date, status 
                      FROM users 
                      WHERE id=?`;

        const queryResult = await conn.query(query, [userId]);
        const loggedinUser: UserResponse | undefined = queryResult[0];
        /* 401: Unauthorized */
        if (!loggedinUser) return res.status(401).send({ message: "Try to logout and login again." });
        /* 200: Send User Information to the frontend */
        return res.status(200).send(loggedinUser);

    } catch (err: unknown) {
        res.status(500).send({ message: "Could not find a user." });
        throw console.log(colors.red(`UserFindbyController.findbyToken => ${err}`));
    } finally {
        if (conn) return conn.release();
    }
};

const UserFindbyController = {
    findbyUsername,
    findbyUserid,
    findbyToken,
};

export default UserFindbyController;
