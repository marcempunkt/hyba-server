import { Request, Response } from "express";
import { PoolConnection } from "mariadb";
import colors from "colors";

import pool from "../db/db";
import type { OkPacket } from "../ts/types/mariadb_types";
import type { PostComment as ServerPostComment } from "../ts/types/db_types";
import type { PostComment as ClientPostComment } from "../ts/types/client_types";
import type { ResponseBody, AuthLocals } from "../ts/types/express_types";

const postComment = async (
    req: Request<{}, {}, { postId: number; content: string }>,
    res: Response<ClientPostComment | ResponseBody, AuthLocals>
) => {
    /**
     * Post a comment ot a Post
     * 
     * @body     {number}  postId
     * @body     {string}  content
     * 
     * @returns  {ClientPostComment}  newComment
     */
    const writtenAt: number = new Date().valueOf();

    let conn: PoolConnection | undefined;

    try {
        conn = await pool.getConnection();
        if (!conn) throw console.error(colors.red("CommentController.postComment() => conn is undefined"));

        /* post comment */
        const queryPostComment: string = `INSERT INTO post_comments (post_id, user_id, content, written_at)
                                     VALUES(?, ?, ?, ?)`;
        const queryPostCommentResult: OkPacket = await conn.query(
            queryPostComment,
            [req.body.postId, res.locals.userId, req.body.content, writtenAt]
        );

        /* retrieve posted comment */
        const queryGetComment: string = `SELECT * FROM post_comments WHERE post_id=? AND user_id=? AND content=? AND written_at=?`;
        const queryGetCommentResult: Array<ServerPostComment> = await conn.query(
            queryGetComment,
            [req.body.postId, res.locals.userId, req.body.content, writtenAt]
        );
        const newComment: ClientPostComment = {
            commentId: queryGetCommentResult[0].id,
            userId: queryGetCommentResult[0].user_id,
            content: queryGetCommentResult[0].content,
            writtenAt: queryGetCommentResult[0].written_at,
        };

        return res.send(newComment);

    } catch(err: unknown) {
        res.status(500).send({ message: "Couldn't post your comment." });
        throw console.error(colors.red(`CommentController.postComment() => ${err}`));
    } finally {
        if (conn) conn.release();
    }
};

const deleteComment = async (
    req: Request<{}, {}, { commentId: number; postId: number }>,
    res: Response<ResponseBody, AuthLocals>
) => {
    /**
     * delete a comment of a post
     * 
     * @param    {number}  commentId
     * @param    {number}  postId
     * 
     * @returns  {Success|Error}
     */
    let conn: PoolConnection | undefined;

    try {
        conn = await pool.getConnection();
        if (!conn) throw console.error(colors.red("CommentController.deleteComment() => conn is undefined"));

        /* only the author of the comment or the author of the post can delete comments */
        /* get author id of the post */
        const queryPostAuthor: string = `SELECT written_by FROM posts WHERE id=?`;
        const queryPostAuthorResult: Array<{ written_by: number }> = await conn.query(queryPostAuthor, [req.body.postId]);
        let postAuthorId: number | undefined;
        if ([...queryPostAuthorResult].length) { postAuthorId = queryPostAuthorResult[0].written_by; }

        /* get author id of the comment */
        const queryCommentAuthor: string = `SELECT user_id FROM post_comments WHERE user_id=? AND post_id=?`;
        const queryCommentAuthorResult: Array<{ user_id: number }> = await conn.query(queryCommentAuthor, [res.locals.userId, req.body.postId]);
        let commentAuthorId: number | undefined;
        if ([...queryCommentAuthorResult].length) { commentAuthorId = queryCommentAuthorResult[0].user_id; }

        /* you aren't allowed to delete this comment */
        if (commentAuthorId !== res.locals.userId && postAuthorId !== res.locals.userId) return res.status(401).send({
            message: `You aren't the author of the comment or the post.`,
        });

        /* delete the comment */
        const queryDeleteComment: string = `DELETE FROM post_comments WHERE id=?`;
        const queryDeleteCommentResult: OkPacket = await conn.query(queryDeleteComment, [req.body.commentId]);

        return res.send({ message: `Comment has been deleted.` });
        
    } catch(err: unknown) {
        res.status(500).send({ message: "Couldn't delete comment." });
        throw console.error(colors.red(`CommentController.deleteComment() => ${err}`));
    } finally {
        if (conn) conn.release();
    }
};

const PostCommentController = {
    postComment,
    deleteComment,
};

export default PostCommentController;
