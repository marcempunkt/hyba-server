import { Router } from "express";

import authentication from "../middlewares/authentication";
import PostController from "../controllers/PostController";
import PostCommentController from "../controllers/PostCommentController";

const router: Router = Router();

// TODO 
// /post
router.get("/", authentication, PostController.getAllPosts);
router.post("/", PostController.postPost);  
router.delete("/", authentication, PostController.deletePost);
router.get("/image/:filename", PostController.getPostImage);
router.patch("/toggle-like", authentication, PostController.toggleLike);

// /post/comment
router.post("/comment/", authentication, PostCommentController.postComment);
router.delete("/comment/", authentication, PostCommentController.deleteComment);

export default router;
