import { Socket, Server as SocketServer } from "socket.io";
import { Server as HttpServer } from "http";
import colors from "colors";

import { updateSocketid } from "./server.utils";
import SocketEventHandler from "./EventHandler";
import { UpdateSocketidEvent,
	 ServerToClientEvents,
	 ClientToServerEvents,
	 InterServerEvents,
	 SocketData } from "../ts/types/socket_types";
import Config from "../lib/config";
import { Message as ClientMessage, Pending, Friend, Post, PostComment } from "../ts/types/client_types";
import { Message as ServerMessage, Status } from "../ts/types/db_types";

type RTCSessionDescription = any;
type RTCSessionDescriptionInit = any;
type RTCIceCandidate = any;

const startSocketServer = async (server: HttpServer, config: Config) => {
    /**
     * the Heart of the socket server
     * all logic on how to handle messages
     * @public
     */
    const io = new SocketServer<ServerToClientEvents, ClientToServerEvents, InterServerEvents, SocketData>(server, {
        pingTimeout:  2000,
        pingInterval: 2000,
        cors: {
            origin: "*",
            methods: ["GET", "POST", "PATCH"],
            credentials: true,
        },
    });

    io.on("connection", async (socket: Socket) => {

        /* Check if the user is authorized */
        const auth: string | string[] | undefined = socket.handshake.query.auth;
        /* typeof array returns "object" */
        if (!auth || typeof auth === "object") {
            console.error(colors.red(`/socket/server.ts => Auth key is null! => ${auth}`));
            return socket.disconnect();
        }

        await updateSocketid(UpdateSocketidEvent.CONNECT, socket, auth, config).catch((err: string) => {
            console.error(colors.red(`/socket/server.ts => updateSocketid: ${err}`));
            return socket.disconnect();
        });

        socket.emit("connected", socket.id);

        /* --- Socket EventListeners/-handlers --- */
        socket.on(
            "disconnect",
            () => SocketEventHandler.handleDisconnect(
	        auth,
	        socket,
	        io,
                config
            )
        );
        /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Messages~ */
        socket.on(
            "send_msg",
            (msg: ServerMessage) => SocketEventHandler.sendMessageHandler(
	        msg,
	        socket,
	        io
            )
        );
        socket.on(
            "delete_msg",
            (deletedMessages: Array<ServerMessage>) => SocketEventHandler.deleteMessageHandler(
	        deletedMessages,
	        socket,
	        io
            )
        );
        socket.on(
            "delete_chat",
            (userId: number, deleteChatUserId: number) => SocketEventHandler.deleteChatHandler(
	        userId,
	        deleteChatUserId,
	        socket,
	        io
            )
        );
        /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Friends~ */
        socket.on(
            "send_friendrequest",
            (fromUserid: number, toUserid: number) => SocketEventHandler.sendFriendrequestHandler(
	        fromUserid,
	        toUserid,
	        socket,
	        io
            )
        );
        socket.on(
            "decline_friendrequest",
            (pending: Pending) => SocketEventHandler.declineFriendrequestHandler(
	        pending,
	        socket,
	        io
            )
        );
        socket.on(
            "accept_friendrequest",
            (pendingId: number, fromUser: number, toUser: number) => SocketEventHandler.acceptFriendrequestHandler(
	        pendingId,
	        fromUser,
	        toUser,
	        socket,
	        io
            )
        );
        socket.on(
            "remove_friend",
            (clientId: number, friendId: number) => SocketEventHandler.removeFriendHandler(
	        clientId,
	        friendId,
	        socket,
	        io
            )
        );
        socket.on(
            "update_avatar",
            (userId: number) => SocketEventHandler.updateAvatarHandler(
	        userId,
	        socket,
	        io
            )
        );
        socket.on(
            "update_status",
            (userId: number, status: Status) => SocketEventHandler.updateStatusHandler(
	        userId,
	        status,
	        socket,
	        io
            )
        );
        socket.on(
            "update_username",
            (userId: number, username: string) => SocketEventHandler.updateUsernameHandler(
	        userId,
	        username,
	        socket,
	        io
            )
        );
        /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Posts~ */
        socket.on(
            "send_post",
            (userId: number, newPost: Post) => SocketEventHandler.sendPostHandler(
	        userId,
	        newPost,
	        socket,
	        io
            )
        );
        socket.on(
            "delete_post",
            (userId: number, postId: number) => SocketEventHandler.deletePostHandler(
	        userId,
	        postId,
	        socket,
	        io
            )
        );
        socket.on(
            "like_or_dislike_post",
            (userId: number, postId: number) => SocketEventHandler.likeOrDislikePostHandler(
	        userId,
	        postId,
	        socket,
	        io
            )
        );
        socket.on(
            "comment_post",
            (userId: number, postId: number, newComment: PostComment) => SocketEventHandler.commentPostHandler(
	        userId,
	        postId,
	        newComment,
	        socket,
	        io
            )
        );
        socket.on(
            "remove_comment_post",
            (userId: number, postId: number, commentId: number) => SocketEventHandler.removeCommentPostHandler(
	        userId,
	        postId,
	        commentId,
	        socket,
	        io
            )
        );
        /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Call/WebRTC~ */
        socket.on(
            "start_calling",
            (data: { fromUser: number; toUser: number; key: string }) => SocketEventHandler.startCallingHandler(
	        data,
	        socket,
	        io
            )
        );
        socket.on(
            "end_calling",
            (data: { fromUser: number; toUser: number; toSocketid?: string }) => SocketEventHandler.endCallingHandler(
	        data,
	        socket,
	        io
            )
        );
        socket.on(
            "client_end_calling",
            (data: { userId: number; calling: number }) => SocketEventHandler.clientEndCallingHandler(
	        data,
	        socket,
	        io
            )
        );
        socket.on(
            "accept_calling",
            (data: { fromUser: number; toUser: number, toSocketid: string }) => SocketEventHandler.acceptCallingHandler(
	        data,
	        socket,
	        io
            )
        );
        socket.on(
            "signaling_icecandidate",
            (data: {
	        fromUser: number;
	        toUser: number;
	        toSocketid: string;
	        candidate: RTCIceCandidate | null;
	        key: string
            }) => SocketEventHandler.signalingIceCandidateHandler(
	        data,
	        socket,
	        io
            )
        );
        socket.on(
            "signaling_offer",
            (data: {
	        fromUser: number;
	        toUser: number;
	        toSocketid: string;
	        offer: RTCSessionDescriptionInit;
	        key: string
            }) => SocketEventHandler.signalingOfferHandler(
	        data,
	        socket,
	        io
            )
        );
        socket.on(
            "signaling_answer",
            (data: {
	        fromUser: number;
	        toUser: number;
	        fromSocketid: string;
	        toSocketid: string;
	        answer: RTCSessionDescriptionInit;
	        key: string
            }) => SocketEventHandler.signalingAnswerHandler(
	        data,
	        socket,
	        io
            )
        );
        socket.on(
            "screenshare_offer",
            (data: {
	        toSocketid: string;
	        offer: RTCSessionDescriptionInit;
	        key: string;
            }) => SocketEventHandler.screenshareOfferHandler(
	        data,
	        socket,
	        io
            )
        );
        socket.on(
            "screenshare_answer",
            (data: {
	        toSocketid: string;
	        answer: RTCSessionDescriptionInit;
	        key: string;
            }) => SocketEventHandler.screenshareAnswerHandler(
	        data,
	        socket,
	        io
            )
        );
        socket.on(
            "screenshare_icecandidate",
            (data: {
	        toSocketid: string;
	        candidate: RTCIceCandidate | null;
	        key: string;
            }) => SocketEventHandler.screenshareIcecandidateHandler(
	        data,
	        socket,
	        io
            )
        );
        socket.on(
            "screenshare_renegotiation_offer",
            (data: {
	        toSocketid: string;
	        offer: RTCSessionDescriptionInit;
	        key: string;
            }) => SocketEventHandler.screenshareRenegotiationOfferHandler(
	        data,
	        socket,
	        io
            )
        );
        socket.on(
            "screenshare_renegotiation_answer",
            (data: {
	        toSocketid: string;
	        answer: RTCSessionDescriptionInit;
	        key: string;
            }) => SocketEventHandler.screenshareRenegotiationAnswerHandler(
	        data,
	        socket,
	        io
            )
        );
        socket.on(
            "screenshare_close",
            (data: {
	        toSocketid: string;
	        key: string;
            }) => SocketEventHandler.screenshareCloseHandler(
	        data,
	        socket,
	        io
            )
        );
        socket.on(
            "hangup",
            (data: { fromUser: number; toUser: number; toSocketid: string }) => SocketEventHandler.hangupHandler(
	        data,
	        socket,
	        io
            )
        );
        socket.on(
            "renegotiation_offer",
            (data: {
	        fromUser: number;
	        toUser: number;
	        toSocketid: string;
	        offer: RTCSessionDescriptionInit;
	        key: string
            }) => SocketEventHandler.renegotiationOfferHandler(
	        data,
	        socket,
	        io
            )
        );
        socket.on(
            "renegotiation_answer",
            (data: {
	        fromUser: number;
	        toUser: number;
	        toSocketid: string;
	        answer: RTCSessionDescriptionInit;
	        key: string
            }) => SocketEventHandler.renegotiationAnswerHandler(
	        data,
	        socket,
	        io
            )
        );
        socket.on(
            "calling_reconnected",
            (data: {
	        key: string;
	        fromUser: number;
	        fromSocketid: string;
	        inCallWith: number;
            }) => SocketEventHandler.callingReconnectedHandler(
	        data,
	        socket,
	        io
            )
        );
        socket.on(
            "calling_reconnected_answer",
            (data: {
	        key: string;
	        fromUser: number;
	        fromSocketid: string;
	        inCallWith: number;
	        inCallWithSocketid: string;
            }) => SocketEventHandler.callingReconnectedAnswerHandler(
	        data,
	        socket,
	        io
            )
        );
    });
};

export default startSocketServer;
