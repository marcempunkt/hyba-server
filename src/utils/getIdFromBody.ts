const getIdFromBody: (bodyId: unknown) => number | null = (bodyId) => {
  const id: number = Number(bodyId);
  if (Number.isInteger(id) && id > 0) return id;
  return null; 
};

export default getIdFromBody;
