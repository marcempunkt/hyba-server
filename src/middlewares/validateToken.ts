import { Request, Response, NextFunction } from "express";
import colors from "colors";
import jwt from "jsonwebtoken";

const validateToken = (req: Request, res: Response, next: NextFunction) => {
  /**
   * Middleware Function to protect the route
   * protected routes need to have req.body.token!!!
   */
  const authErrorMsg = "Authentication Error: Please try to log out and in again";

  const token: string | undefined = req.body.token || req.params.token;
  if (!token) return res.status(401).send({ error: authErrorMsg });
  if (!process.env.SECRET_TOKEN) return res.status(500).send({ error: authErrorMsg });

  try {
    const validToken: boolean = (jwt.verify(token, process.env.SECRET_TOKEN)) ? true : false;
    if (validToken) return next();
    return res.status(401).send({ error: authErrorMsg });
  } catch(err) {
    console.error(colors.red(`/api/validate.validationToken => ${err}`));
    return res.status(500).send({ error: authErrorMsg });
  }
}

export default validateToken;
