import { Application } from "express";
import cors from "cors";
import colors from "colors";

const app = (app: Application) => {
    if (process.env.NODE_ENV === "dev") {
        console.warn(colors.yellow("CORS Middleware is enabled."));
        app.use(cors());
    }
};

export default { app };
