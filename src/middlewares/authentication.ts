import { Request, Response, NextFunction } from "express"
import jwt, { JwtPayload } from "jsonwebtoken";
import colors from "colors";
import Config from "../lib/config";

const authentication = async (req: Request, res: Response, next: NextFunction) => {
    /**
     * Middleware to validate jwt auth token
     * that is stored inside req.header
     */
    const authErrorMsg = "Authentication Error: Please try to log out and in again";
    // @ts-ignore : I don't know how to type req.app correctly
    const config: Config = req.app.locals.config as Config;

    const token: string | undefined = req.get("Authorization")?.split(" ")[1];
    if (!token) return res.status(401).send({ message: authErrorMsg });

    const valid: boolean = (jwt.verify(token, config.auth.secret)) ? true : false;
    if (!valid) return res.status(401).send({ message: authErrorMsg });

    /** get user id from token */
    const jwtPayload: string | JwtPayload | null = jwt.decode(token);
    if (!jwtPayload ||
        typeof jwtPayload === "string" ||
        !jwtPayload.id ||
        jwtPayload.id < 0 ||
        !Number.isInteger(jwtPayload.id)) return res.status(401).send({ message: authErrorMsg });

    /** store user id inside locals */
    res.locals.userId = jwtPayload.id;
    next();
};

export default authentication;
