#!/bin/bash
## Prodcution Environment
turnserver --fingerprint \
	   --verbose \
	   --listening-port 3478 \
	   --tls-listening-port 5349 \
	   --lt-cred-mech \
	   --server-name sturn.babbeln.app \
	   --realm babbeln.app \
	   --user sturnbabbelnapp:sturnbabbelnapppassword \
	   --external-ip $EXTERNAL_IP \
	   --min-port 49152 \
	   --max-port 65535 \
	   --cert /turnssl/cert.pem \
	   --pkey /turnssl/privkey.pem \
	   --log-file /var/log/turnserver/turn.log \
	   --pidfile /run/turnserver/turnserver.pid
