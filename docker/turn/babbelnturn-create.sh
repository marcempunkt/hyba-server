#!/bin/bash

docker run \
       --name babbelnturn \
       --network host \
       -d coturn/coturn \
       -n \
       --listening-port=3478 \
       --min-port=49152 --max-port=65535 \
       --external-ip='$(detect-external-ip)' \
       --verbose \
       --fingerprint \
       --lt-cred-mech \
       --server-name=turn.babbeln.app \
       --realm=babbeln.app \
       --user=turnbabbelnapp:turnbabbelnapppassword \
       --log-file=stdout \
       --pidfile=/run/turnserver/turnserver.pid

# todo
# -v $PWD/docker/turn/data/log:/var/log/turnserver/turn.log \
# --log-file=/var/log/turnserver/turn.log \

       # -v $PWD/docker/turn/data/:
       # --cert /turnssl/cert.pem \
       # --pkey /turnssl/privkey.pem \


       # --tls-listening-port=5349 \

       # -p 3478:3478 \
       # -p 5349:5349 \
       # -p 3478:3478/udp \
       # -p 5349:5349/udp \
       # -p 49152-65535:49152-65535/udp \

# CONFIGURATION ALERT: Unknown argument: /home/wjatscheslaw/Documents/Projects/babbeln-server/docker/turn/data/log:/var/log/turnserver/turn.log
# 0: : ERROR:
# CONFIG ERROR: Empty cli-password, and so telnet cli interface is disabled! Please set a non empty cli-password!
# 0: : WARNING: cannot find certificate file: turn_server_cert.pem (1)
# 0: : WARNING: cannot start TLS and DTLS listeners because certificate file is not set properly
# 0: : WARNING: cannot find private key file: turn_server_pkey.pem (1)
# 0: : WARNING: cannot start TLS and DTLS listeners because private key file is not set properly
