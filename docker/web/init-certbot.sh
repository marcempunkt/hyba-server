#!/bin/bash
#### --------------------------------------------- Check if docker-compose is installed
if ! [ -x "$(command -v docker-compose)" ]; then
  echo 'Error: docker-compose is not installed.' >&2
  exit 1
fi

#### --------------------------------------------- Variables
domains_babbelnapp=(babbeln.app www.babbeln.app)
domains_serverbabbelnapp=(server.babbeln.app www.server.babbeln.app)
domains_turnbabbelnapp=(turn.babbeln.app www.turn.babbeln.app)
rsa_key_size=4096
data_path="./docker/web/data/certbot"
email="marc.maeurer@pm.me" # Adding a valid address is strongly recommended # TODO add email to dotenv
staging=0 # Set to 1 if you're testing your setup to avoid hitting request limits

#### --------------------------------------------- Check if there are already certs
if [ -d "$data_path" ]; then
  read -p "Existing data found for $domains. Continue and replace existing certificate? (y/N) " decision
  if [ "$decision" != "Y" ] && [ "$decision" != "y" ]; then
    exit
  fi
fi

#### --------------------------------------------- Download options-ssl-nginx.conf & ssl-dhparams.pem
if [ ! -e "$data_path/conf/options-ssl-nginx.conf" ] || [ ! -e "$data_path/conf/ssl-dhparams.pem" ]; then
  echo "### Downloading recommended TLS parameters ..."
  mkdir -p "$data_path/conf"
  curl -s https://raw.githubusercontent.com/certbot/certbot/master/certbot-nginx/certbot_nginx/_internal/tls_configs/options-ssl-nginx.conf > "$data_path/conf/options-ssl-nginx.conf"
  curl -s https://raw.githubusercontent.com/certbot/certbot/master/certbot/certbot/ssl-dhparams.pem > "$data_path/conf/ssl-dhparams.pem"
  echo
fi

#### --------------------------------------------- Create dummy certificates 
## ----------------- Babbeln.app
echo "### Creating dummy certificate for $domains_babbelnapp ..."
path_babbelnapp="/etc/letsencrypt/live/$domains_babbelnapp"
mkdir -p "$data_path/conf/live/$domains_babbelnapp"
docker-compose -f ./docker/web/docker-compose.yaml run --rm --entrypoint "\
  openssl req -x509 -nodes -newkey rsa:$rsa_key_size -days 1\
    -keyout '$path_babbelnapp/privkey.pem' \
    -out '$path_babbelnapp/fullchain.pem' \
    -subj '/CN=localhost'" babbelnweb-certbot
echo
## ----------------- server.Babbeln.app
echo "### Creating dummy certificate for $domains_serverbabbelnapp ..."
path_serverbabbelnapp="/etc/letsencrypt/live/$domains_serverbabbelnapp"
mkdir -p "$data_path/conf/live/$domains_serverbabbelnapp"
docker-compose -f ./docker/web/docker-compose.yaml run --rm --entrypoint "\
  openssl req -x509 -nodes -newkey rsa:$rsa_key_size -days 1\
    -keyout '$path_serverbabbelnapp/privkey.pem' \
    -out '$path_serverbabbelnapp/fullchain.pem' \
    -subj '/CN=localhost'" babbelnweb-certbot
echo
## ----------------- turn.Babbeln.app
echo "### Creating dummy certificate for $domains_turnbabbelnapp ..."
path_turnbabbelnapp="/etc/letsencrypt/live/$domains_turnbabbelnapp"
mkdir -p "$data_path/conf/live/$domains_turnbabbelnapp"
docker-compose -f ./docker/web/docker-compose.yaml run --rm --entrypoint "\
  openssl req -x509 -nodes -newkey rsa:$rsa_key_size -days 1\
    -keyout '$path_turnbabbelnapp/privkey.pem' \
    -out '$path_turnbabbelnapp/fullchain.pem' \
    -subj '/CN=localhost'" babbelnweb-certbot
echo

#### --------------------------------------------- Start nginx-master container
echo "### Starting nginx ..."
docker-compose -f ./docker/web/docker-compose.yaml up --force-recreate -d babbelnweb-nginx-master
echo

#### --------------------------------------------- Delete dummy certs
echo "### Deleting dummy certificate for $domains_babbelnapp ..."
docker-compose -f ./docker/web/docker-compose.yaml run --rm --entrypoint "\
  rm -Rfv /etc/letsencrypt/live/$domains_babbelnapp && \
  rm -Rfv /etc/letsencrypt/archive/$domains_babbelnapp && \
  rm -Rfv /etc/letsencrypt/renewal/$domains_babbelnapp.conf" babbelnweb-certbot
echo
echo "### Deleting dummy certificate for $domains_serverbabbelnapp ..."
docker-compose -f ./docker/web/docker-compose.yaml run --rm --entrypoint "\
  rm -Rfv /etc/letsencrypt/live/$domains_serverbabbelnapp && \
  rm -Rfv /etc/letsencrypt/archive/$domains_serverbabbelnapp && \
  rm -Rfv /etc/letsencrypt/renewal/$domains_serverbabbelnapp.conf" babbelnweb-certbot
echo
echo "### Deleting dummy certificate for $domains_turnbabbelnapp ..."
docker-compose -f ./docker/web/docker-compose.yaml run --rm --entrypoint "\
  rm -Rfv /etc/letsencrypt/live/$domains_turnbabbelnapp && \
  rm -Rfv /etc/letsencrypt/archive/$domains_turnbabbelnapp && \
  rm -Rfv /etc/letsencrypt/renewal/$domains_turnbabbelnapp.conf" babbelnweb-certbot
echo

#### --------------------------------------------- Create let's encrypt certs
# Select appropriate email arg
case "$email" in
  "") email_arg="--register-unsafely-without-email" ;;
  *) email_arg="--email $email" ;;
esac

# Enable staging mode if needed
if [ $staging != "0" ]; then staging_arg="--staging"; fi

## ----------------- babbeln.app
echo "### Requesting Let's Encrypt certificate for $domains_babbelnapp ..."
domain_babbelnapp_args=""
for domain in "${domains_babbelnapp[@]}"; do
  domain_babbelnapp_args="$domain_babbelnapp_args -d $domain"
done

docker-compose -f ./docker/web/docker-compose.yaml run --rm --entrypoint "\
  certbot certonly --webroot -w /var/www/ \
    $staging_arg \
    $email_arg \
    $domain_babbelnapp_args \
    --rsa-key-size $rsa_key_size \
    --agree-tos \
    --force-renewal" babbelnweb-certbot
echo

## ----------------- server.babbeln.app
echo "### Requesting Let's Encrypt certificate for $domains_serverbabbelnapp ..."
domain_serverbabbelnapp_args=""
for domain in "${domains_serverbabbelnapp[@]}"; do
  domain_serverbabbelnapp_args="$domain_serverbabbelnapp_args -d $domain"
done

docker-compose -f ./docker/web/docker-compose.yaml run --rm --entrypoint "\
  certbot certonly --webroot -w /var/www/ \
    $staging_arg \
    $email_arg \
    $domain_serverbabbelnapp_args \
    --rsa-key-size $rsa_key_size \
    --agree-tos \
    --force-renewal" babbelnweb-certbot
echo

## ----------------- turn.babbeln.app
echo "### Requesting Let's Encrypt certificate for $domains_turnbabbelnapp ..."
domain_turnbabbelnapp_args=""
for domain in "${domains_turnbabbelnapp[@]}"; do
  domain_turnbabbelnapp_args="$domain_turnbabbelnapp_args -d $domain"
done

docker-compose -f ./docker/web/docker-compose.yaml run --rm --entrypoint "\
  certbot certonly --webroot -w /var/www/ \
    $staging_arg \
    $email_arg \
    $domain_turnbabbelnapp_args \
    --rsa-key-size $rsa_key_size \
    --agree-tos \
    --force-renewal" babbelnweb-certbot
echo

#### --------------------------------------------- Reload nginx-master with the let's encrypt SSL Certificates
echo "### Reloading nginx ..."
docker-compose -f ./docker/web/docker-compose.yaml exec babbelnweb-nginx-master nginx -s reload
